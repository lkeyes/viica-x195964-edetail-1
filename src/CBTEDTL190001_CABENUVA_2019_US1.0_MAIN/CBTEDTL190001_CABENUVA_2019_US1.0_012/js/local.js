// --- local.js --- //
let selectionType = 'leadin';
let initialState = true;

const bufferPopup = $("#buffer-popup");
const bufferInfo = $('#buffer-info');
const invalidInjectionPopup= $('#invalidInjection-popup');
const invalidLeadinPopup = $('#invalidLeadin-popup');
const invalidInjectionCalendarPopup= $('.invalidInjection-popup');
const invalidLeadinCalendarPopup = $('.invalidLeadin-popup');

const openBufferPopup = ()=>{
    bufferPopup.removeClass('hidden');
};
const closeBufferPopup = ()=>{
    bufferPopup.addClass('hidden');
};
const closePopups = ()=>{
 
    if(invalidInjectionPopup.is(':visible')){   
        invalidInjectionPopup.addClass('hidden');
    }
    if(invalidInjectionCalendarPopup.is(':visible')){
        invalidInjectionCalendarPopup.addClass('hidden');
    }
    if(invalidLeadinPopup.is(':visible')){
        invalidLeadinPopup.addClass('hidden');
    }
    if(invalidLeadinCalendarPopup.is(':visible')){
        invalidLeadinCalendarPopup.addClass('hidden');
    }
    if(bufferPopup.is(':visible')){
        bufferPopup.addClass('hidden');
    }
};

// com.gsk.mt.blockSwipes = false;

com.gsk.mt.onInit = function () {
    let currentDate = new Date();

    com.gsk.mt.customSwipe({
        "rightSlide": "CBTEDTL190001_CABENUVA_2019_US1.0_011",
        "leftSlide": "CBTEDTL190001_CABENUVA_2019_US1.0_013"
    });

    // initialize();

    generateMonths(currentDate);
    $('.selection-toggle button').click(($e) => handleSelectionToggle($e));
    // $('.calendar .dialog button').click(($e) => handleInvalidDialogButtonClick($e));
    $('.reset').click(($e) => handleReset($e));
};

function generateMonths(date) {

    let months = [];

    for (let m = 0; m < 4; m++) {
        // console.log(i);
        let activeDate;
        if (selectionType === 'injection' && date.getDate() < 28 && !initialState) {
            activeDate = new Date(date.getFullYear(), date.getMonth() + m - 1, 1);
        } else {
            activeDate = new Date(date.getFullYear(), date.getMonth() + m, 1);
        }

        let daysInMonth = [];

        for (let d = 0; d < activeDate.getDaysInMonth(); d++) {
            const currentDay = new Date(activeDate.getFullYear(), activeDate.getMonth(), d + 1);
            daysInMonth.push(currentDay);
        }

        let newMonth = new Month(activeDate.getMonthName(), daysInMonth);

        months.push(newMonth);
    }

    // generate dom for each calendar month
    months.forEach((month) => {
        // month container
        let monthEl = $('<div>').addClass('month');
        const monthNameEl = $('<p>')
            .addClass('name')
            .text(month.name);

        // key container
        const daysKeyEl = $('<div>').addClass('key');
        const keyElements = ['SU', 'MO', 'TU', 'WE', 'TH', 'FR', 'SA'];
        keyElements.forEach(el => {
            let keyEl = $('<p>').text(el);
            $(daysKeyEl).append(keyEl)
        });

        // days container
        const daysEl = $('<div>').addClass('days');
        const leadingSpacerDays = month.days[0].getDay();
        // console.log('The first day is', month.days[0], ' which is a', leadingSpacerDays);
        for (let d = 0; d < leadingSpacerDays; d++) {
            const spacerDateEl = $('<p>')
                .addClass('day');
            $(daysEl).append(spacerDateEl);
        }
        month.days.forEach(day => {
            // this is used to determine 
            let offsetSelectedDate = new Date(date);
            let offsetActiveDate = new Date(day);

            const dateEl = $('<p>')
                .text(day.getDate())
                .addClass('day')
                .click(($e) => {
                    handleDaySelection(day);
                });




            switch (selectionType) {

                case 'injection':
                    const isInvalidInjectionDate = day.getDate() > 28;
                    if (isInvalidInjectionDate && initialState) {
                        $(dateEl).addClass('invalid');
                    }

                    break;

                case 'leadin':
                    const injectionDateFromThisDay = new Date(day.getFullYear(), day.getMonth(), day.getDate() + 27);
                    const isInvalidLeadinDate = injectionDateFromThisDay.getDate() > 28;
                    if (isInvalidLeadinDate && initialState) {
                        $(dateEl).addClass('invalid');
                    }

                    offsetSelectedDate.setDate(offsetSelectedDate.getDate() + 27);
                    break;
            }

            if (selectionType && !initialState) {
                const isWithinThreeMonths = offsetActiveDate < new Date(offsetSelectedDate.getFullYear(), offsetSelectedDate.getMonth() + 3);
                const isFirstInjectionDate = offsetActiveDate.getTime() === offsetSelectedDate.getTime();
                const isInjectionDate = offsetActiveDate.getDate() === offsetSelectedDate.getDate() && (isFirstInjectionDate || offsetActiveDate > offsetSelectedDate);
                const isNotInFirstMonth = offsetActiveDate > new Date(offsetSelectedDate.getFullYear(), offsetSelectedDate.getMonth(), offsetSelectedDate.getDate() + 7);
                const dayBeforeLeadInDates = new Date(offsetSelectedDate);
                dayBeforeLeadInDates.setDate(dayBeforeLeadInDates.getDate() - 28);
                const isLeadInDate = dayBeforeLeadInDates < offsetActiveDate && offsetActiveDate < offsetSelectedDate;
                let isBufferDate;

                // Check for buffer date
                for (let d = 1; d < 8; d++) {
                    const earlierDay = new Date(offsetActiveDate);
                    earlierDay.setDate(earlierDay.getDate() - d);
                    const laterDay = new Date(offsetActiveDate);
                    laterDay.setDate(laterDay.getDate() + d);

                    if (earlierDay.getDate() === offsetSelectedDate.getDate() || laterDay.getDate() === offsetSelectedDate.getDate()) {
                        isBufferDate = true;
                        break;
                    }
                }

                if (isInjectionDate) {
                    $(dateEl).addClass('injection');
                    if (isFirstInjectionDate) {
                        $(dateEl).addClass('first');
                    }
                } else if (!isInjectionDate && isBufferDate && isNotInFirstMonth) {
                    $(dateEl).addClass('buffer');
                } else if (isLeadInDate) {
                    $(dateEl).addClass('lead-in');
                }
            }

            $(daysEl).append(dateEl);
        });

        $(monthEl).append([monthNameEl, daysKeyEl, daysEl]);

        $('.calendar').append(monthEl);
    });

    updateKey();
    if (initialState) {
        $('.calendar').removeClass('disabled');
    }

    return months;

}

function updateKey() {
    if (initialState) {
        switch (selectionType) {
            case 'injection':
                $('.legend .invalid .description').text('Not recommended for injection');
                break;
            case 'leadin':
                $('.legend .invalid .description').text('Not recommended for oral lead-in start');
                break;
        }
    }
}

// Handlers
function handleSelectionToggle($e) {
    $('.selection-toggle').children().removeClass('active');
    closePopups();
    selectionType = $e.target.dataset.selectionType;
    $($e.target).addClass('active');
    handleReset($e);
}

function handleReset($e) {
    initialState = true;
    $('.reset').hide();
    $('.calendar .month').remove();
    generateMonths(new Date());
}

// function handleInvalidDialogButtonClick($e) {
//     $('.calendar .dialog').addClass('hidden');
// }

function handleDaySelection(day) {
    const injectionDateFromThisDay = new Date(day.getFullYear(), day.getMonth(), day.getDate() + 27);
    const isInvalidLeadinDate = injectionDateFromThisDay.getDate() > 28;
    const isInvalidInjectionDate = day.getDate() > 28;

    // if (selectionType == 'injection' && isInvalidInjectionDate) {
    //     $('.calendar .invalid-injection').removeClass('hidden');
    // } else if (selectionType == 'leadin' && isInvalidLeadinDate) {
    //     $('.calendar .invalid-leadin').removeClass('hidden');

    if (selectionType == 'injection' && isInvalidInjectionDate) {
        $('.calendar .invalidInjection-popup').removeClass('hidden');
    } else if (selectionType == 'leadin' && isInvalidLeadinDate) {
        $('.calendar .invalidLeadin-popup').removeClass('hidden');
    } else {
        initialState = false;
        $('.reset').show();
        $('.calendar').addClass('disabled');
        $('.calendar .month').remove();
        generateMonths(day);
        $('.calendar .invalidInjection-popup, .calendar .invalidLeadin-popup').addClass('hidden');
    }
}

// Prototypes
function Month(name, days) {
    this.name = name;
    this.days = days;
}

Date.prototype.getDaysInMonth = function () {
    let year = this.getFullYear();
    let month = this.getMonth();
    let totalDays = new Date(year, month + 1, 0).getDate();
    return totalDays;
}

Date.prototype.getMonthName = function () {

    let monthName;
    switch (this.getMonth()) {
        case 0:
            monthName = 'January';
            break;
        case 1:
            monthName = 'February';
            break;
        case 2:
            monthName = 'March';
            break;
        case 3:
            monthName = 'April';
            break;
        case 4:
            monthName = 'May';
            break;
        case 5:
            monthName = 'June';
            break;
        case 6:
            monthName = 'July';
            break;
        case 7:
            monthName = 'August';
            break;
        case 8:
            monthName = 'September';
            break;
        case 9:
            monthName = 'October';
            break;
        case 10:
            monthName = 'November';
            break;
        case 11:
            monthName = 'December';
            break;
    }
    return monthName;
}

// function initialize() {
//     $("#dosingSchedule").removeClass("inActive");
//     $('#dosingSchedule').addClass("active");
//     $("#getCabenuva").addClass("inActive");
//     $('#dosingCalendar').addClass("inActive");
//     $('#pkProfile').addClass("inActive");
// }

$(document).ready(function () {


    // var i = 0;

    // $("#logo").on("click", function () {
    //     if (i == 0) {
    //         initialize();
    //         i++;
    //     } else if (i == 1) {
    //         $("#dosingSchedule").removeClass("active");
    //         $('#dosingSchedule').addClass("inActive");
    //         $("#getCabenuva").removeClass("inActive");
    //         $("#getCabenuva").addClass("active");
    //         i++;
    //     } else if (i == 2) {
    //         $("#getCabenuva").removeClass("active");
    //         $('#getCabenuva').addClass("inActive");
    //         $("#dosingCalendar").removeClass("inActive");
    //         $("#dosingCalendar").addClass("calActive");
    //         i++;
    //     } else {
    //         $("#dosingCalendar").removeClass("calActive");
    //         $('#dosingCalendar').addClass("inActive");
    //         $("#pkProfile").removeClass("inActive");
    //         $("#pkProfile").addClass("active");
    //         i = 0;
    //     }
    // }); // logo onClick

    // Local Storage setup
    // if (localStorage.getItem('view') == 0) {
    //     $("#dosingSchedule").trigger("click");
    // }

    // if (localStorage.getItem('view') == 2) {
    //     $("#getCabenuva").trigger("click");
    // }

    // if (localStorage.getItem('view') == 3) {
    //     $("#dosingCalendar").trigger("click");
    // }

    // if (localStorage.getItem('view') == 4) {
    //     $("#pkProfile").trigger("click");
    // }

    // localStorage.clear();

    // logic for Pop ups

    // $("#buffer-info").on("click", function () {
    //     $("#buffer-popup").removeClass("inActive");
    //     $("#buffer-popup").addClass("active");
    // });
    // $("#buffer-popup").on("click", function () {
    //     $("#buffer-popup").removeClass("active");
    //     $("#buffer-popup").addClass("inActive");
    // });


    bufferInfo.click(openBufferPopup);
    bufferPopup.click(closeBufferPopup);

    $("#invalid-info").on("click", function () {
        if (selectionType == 'injection') {
            $('#invalidInjection-popup').removeClass('hidden');
        } else if (selectionType == 'leadin') {
            $('#invalidLeadin-popup').removeClass('hidden');
        }
    });
    $("#invalidLeadin-popup").on("click", function () {
        $("#invalidLeadin-popup").addClass("hidden");
    });
    $("#invalidInjection-popup").on("click", function () {
        $("#invalidInjection-popup").addClass("hidden");
    });
    $(".calendar .invalidInjection-popup").on("click", function () {
        $(".calendar .invalidInjection-popup").addClass("hidden");
    });
    $(".calendar .invalidLeadin-popup").on("click", function () {
        $(".calendar .invalidLeadin-popup").addClass("hidden");
    });

    init();

}); // doc ready

function init() {
    const sectionName = sessionStorage.getItem('slideSection');
    // console.log(sectionName);
    let section;
    switch (sectionName) {
        case null:
            section = 0;
            break;
        case 'dosing-implementation':
            section = 1;
            break;
        case 'dosing-calendar':
            section = 2;
            break;
        case 'dosing-pkp':
            section = 3;
            break;
    }

    updateReferences(section)
    handleSectionReveal(section);
    sessionStorage.removeItem('slideSection');
}

function handleSectionReveal(section) {
    const sections = ['#dosingSchedule','#getCabenuva',"#dosingCalendar","#pkProfile"];
    const activeClasses = ['.active', '.calActive'];
    // console.log(section);
    $(sections.join(',')).removeClass(activeClasses.join(','));
    switch (section) {
        case 0:
            $('#dosingSchedule').addClass("active");
			$('#isidefault').css('display','block');
            break;
        case 1:
            $("#getCabenuva").addClass("active");
			$('#isigetcabenuva').css('display','block');
            break;
        case 2:
            $("#dosingCalendar").addClass("calActive");
			$('#isicalendar').css('display','block');
            break;
        case 3:
            $("#pkProfile").addClass("active");
			$('#isipkprofile').css('display','block');
            break;
    }
}

function updateReferences(section){

    let refElement = $('sup.gotoRef.logEmbedded')//.removeAttr('data-reftarget')

    switch (section) {
        case 0:
        refElement.attr('data-reftarget', '1')
        break;
    case 1:
        refElement.attr('data-reftarget', '1')
        break;
    case 2:
        refElement.attr('data-reftarget', '1')
        break;
    case 3:
        refElement.attr('data-reftarget', '1,3')
        break;
    }

}