// --- local.js --- //

com.gsk.mt.onInit = function() {

    const circumference = 559.2;

    $( function() {

        
        const toggleBtn = document.querySelector('#toggle-btn')
        const valueDisplay = document.querySelector('#value-display');
        const centralCircle = document.querySelector('#central-circle-foreground');
        const centralCircleSlider = document.querySelector('#central-circle-slider');
        const dChart = document.querySelector('#d-chart');
        const chartGlow = document.querySelector('#background-glow');
        const displayContainer = document.querySelector('.central-display-container');
        
        const hmmToggleBtn = new Hammer(toggleBtn);
        let isInRevealState = false;
        const prefSlider = $( "#slider");
        //$('#dynamic-content .reveal-state').toggle();
        
        const outputDisplay = `<p class="fnt-85">98<span class="fnt-71">%</span> </p><p class="fnt-17">(n=523/532)<br/> of respondents<br/> preferred CABENUVA</p>`;

        const toggleStateVisibiliy = ()=>{
            
            $('#dynamic-content .reveal-state').toggle();
            $('#dynamic-content .interaction-state').toggle();
        }


        hmmToggleBtn.on('tap', ()=>{

            isInRevealState = !isInRevealState;
        

            if(isInRevealState){
                toggleBtn.innerHTML = 'Reset'
                prefSlider.slider({'value': 88, disabled: true});
                valueDisplay.innerHTML= `<p>88<span class="precent-font">%</span></p>`;
                //centralCircle.innerHTML= outputDisplay;
                valueDisplay.style.cssText = `opacity : 1`;
                //centralCircleSlider.style.cssText = `left : 98%`;
                //$(centralCircleSlider).removeClass('circle-shadow');
                //$(centralCircle).removeClass('enlarged-font');
                $(centralCircleSlider).addClass('final-state');
                $(chartGlow).removeClass('default-state');
                centralCircle.innerHTML= `<p>88<span class="precent-font">%</span></p>`;


                $(displayContainer).addClass('show-border');

                dChart.style.cssText = `stroke-dashoffset: ${circumference}`;
                $(dChart).hide();
                $('#slider').addClass('smooth-slide');

                toggleStateVisibiliy();

            }
            else{
                toggleBtn.innerHTML  = 'Reveal'
                prefSlider.slider({'value': 0, disabled: false});
                valueDisplay.innerHTML= 0;
                centralCircle.innerHTML= `<span class="enlarged-font">?</span>`;
                valueDisplay.style.cssText = `opacity : 0`;
                //centralCircleSlider.style.cssText = `left : 0%`;
                //$(centralCircleSlider).addClass('reveal-state');
                //$(centralCircle).addClass('enlarged-font');
                //$(chartGlow).addClass('default-state');
                $(centralCircleSlider).removeClass('final-state');

                //displayContainer.removeClass('show-border');
                $(displayContainer).removeClass('show-border');

                dChart.style.cssText = `stroke-dashoffset: ${circumference}`;
                $('#slider').removeClass('smooth-slide');

                toggleStateVisibiliy();
            }

        })



        prefSlider.slider({
            range: "min",
            min: 0,
            max: 100,
            create: (event, ui)=>{
                $('.ui-ticks').show();
            },
            slide : (event, ui)=>{
                // valueDisplay.style.cssText = `opacity : 0`;
                // centralCircle.innerHTML= "";
                //disable swipe
                com.gsk.mt.blockSwipes = true;
                // console.log('sliding');

                let val = ui.value;

                

                let flipVal = 1-(val *.01);

                let chartVal = circumference*flipVal;

                valueDisplay.innerHTML=`<p>${val}<span class="precent-font">%</span></p>`;
                valueDisplay.style.cssText = `opacity : 1`;
                //centralCircleSlider.style.cssText = `left : ${val}%`;

                centralCircle.innerHTML= `<p>${val}<span class="precent-font">%</span></p>`;

                dChart.style.cssText = `stroke-dashoffset: ${chartVal}`;


            },
            stop: ( event, ui )=>{

                // Moved the following code to the slide event rather than here
                // so there is a nicer animated and realtime update effect. Take it
                // out from above and uncomment here if it causes issues
                
                // let val = ui.value;

                

                // let flipVal = 1-(val *.01);

                // let chartVal = circumference*flipVal;

                // valueDisplay.innerHTML=`<p>${val}<span class="precent-font">%</span></p>`;
                // valueDisplay.style.cssText = `opacity : 1`;
                // //centralCircleSlider.style.cssText = `left : ${val}%`;

                // centralCircle.innerHTML= `<p>${val}<span class="precent-font">%</span></p>`;

                // dChart.style.cssText = `stroke-dashoffset: ${chartVal}`;



                //enable swipe
                com.gsk.mt.blockSwipes = false;
            }
        });
    });
};