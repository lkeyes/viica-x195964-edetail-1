// --- local.js --- //

com.gsk.mt.onInit = function() {

    $('.toggle-panel').on('click', function(e){

        e.preventDefault();

        var panel_class = $(this).attr('data-panel'),
            panel = $('.panel.' + panel_class);
        
        $.when(
            $('.panel').not(panel).fadeOut()
        ).then(function(){
           panel.fadeIn();
        });

    });

};