
// Tell GSK MT to wait (via jQuery doc ready emitter)
$.holdReady(true);

let elementPromises = [];

// Put elements here
const globalElements = [
    {
        selector: '.navBottom',
        path: '../shared/media/content/nav-bottom.html'
    }
];

const dialogs = [
    {
        selector: '<div id="safe-dialog" style="padding: 0; align-items: center; justify-content: center" class="dialog noTitlebar hidden scrollable" data-description="Parent popup"></div>',
        path: '../shared/media/content/dialogs/dialogISI.html'
    }
]

const version = {
    selector: '<div class="app-version"></div>',
    path: '../shared/media/content/version.html'
}

// Iterate over elements and generate promises for each
globalElements.forEach(el => {
    let loaderPromise = new Promise((resolve, reject) => {
        $(el.selector).load(el.path, () => {
            resolve();
        });
    });
    elementPromises.push(loaderPromise);
});

dialogs.forEach(el => {
    let loaderPromise = new Promise((resolve, reject) => {
        let dialog = $(el.selector);
        $(dialog).load(el.path, () => {
            $('#container').append(dialog);
            resolve();
        });
    });
    elementPromises.push(loaderPromise);
});

elementPromises.push(new Promise((resolve, reject) => {
    let versionEl = $(version.selector);
    $(versionEl).load(version.path, () => {
        $('#container').append(versionEl);
        resolve();
    });
}))

// Await resolution of all promises before allowing docready to fire
Promise.all(elementPromises).then(() => {
    // console.log('All elements loaded');
 
    $.holdReady(false);
});
