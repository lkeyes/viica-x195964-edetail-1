/**********************************************************/
/* GSK Veeva Master Template - Presentation Functionality */
/**********************************************************/
/* File version              1.7                          */
/* Last modified             03/12/2018                   */
/* Last modified by          Design Center                */
/**********************************************************/

// --- CUSTOM TO THIS PRESENTATION --- //

$(document).ready(function () {
    //console.log('doc ready called')

    if ($("#setAnimations").length) {
        // --- SET TEXT ON HOME PAGE BUTTON --- //
        if (window.sessionStorage.getItem('mtgskAnimations') === '1') {
            $('#setAnimations').text("Animations are On");
        } else {
            $('#setAnimations').text("Animations are Off");
        }

        com.gsk.mt.bindInteraction("#setAnimations", "tap", {}, function () {
            if (window.sessionStorage.getItem('mtgskAnimations') === '1') {
                window.sessionStorage.setItem('mtgskAnimations', '0');
                $('#setAnimations').text("Animations are Off");
                com.gsk.mt.debug("Animations are Off");
            } else {
                window.sessionStorage.setItem('mtgskAnimations', '1');
                $('#setAnimations').text("Animations are On");
                com.gsk.mt.debug("Animations are On");
            }
        });
    }


    $(".isi-right #expand-btn").on("click", function () {
        $(".isi-footer").css("height", "36.5%");

        $("#expand-btn").toggle();
        $("#collapse-btn").toggle();
    });
    $(".isi-right #collapse-btn").on("click", function () {
        $(".isi-footer").css("height", "26vh");
        $("#expand-btn").toggle();
        $("#collapse-btn").toggle();
    });

    let $bottomBar = $('<div class="bottom-bar"></div>');
    $('.ui-dialog').append($bottomBar);



    // BEGIN CUSTOM MENU MODS
    $('#customMenu .customMenu').load('../shared/media/content/custom-menu.html');

    // END CUSTOM MENU MODS


    removeSlideRefs();


    // version number logic
    const hideVersion = JSON.parse(sessionStorage.getItem('hideVersion'));
    if (!hideVersion) {
        $('.app-version').show();
    }
    $('.app-version').click(($e) => {
        $($e.target).hide();
        sessionStorage.setItem('hideVersion', true)

    });


});

const removeSlideRefs = () => {
    //Remove References for specific kms
    const kmsWithoutRefs = ['010'];
    const currentKm = com.gsk.mt.currentSlide.split('_').pop();
    const currentKmHasNoRefs = kmsWithoutRefs.indexOf(currentKm) > -1;
    const refEl = $('#references');

    if (currentKmHasNoRefs) {
        refEl.addClass('noReferences activeNav');
    }
}



/* References Popup - Pluralization */
function singularizeRefsHeaders() {

    const regEl = $('sup.gotoRef.logEmbedded');
    const refList = regEl.attr('data-reftarget').split(',');

    //var nRefs = JSON.parse(sessionStorage.getItem("mtgskSlideRefTarget")).references.length;

    if (refList <= 1) {
        $(".embeddedReferences h3").text("Reference");
    } else {
        $(".embeddedReferences h3").text("References");
    }
}

//Tabs functions
const handleTab1 = () => {
    $(".h1-header--tab").removeClass("active");
    $(".tab-2").removeClass("active");
    $(".tab-3").removeClass("active");
    $(".side-menu__item").removeClass("active");
    $("#tab_1_button").addClass("active");
    $(".tab").removeClass("active");
    $(".tab-1").addClass("active");
    $(".special-adjust-tab1-footnotes").addClass("active");
}

const handleTab2 = () => {
    $(".h1-header--tab").removeClass("active");
    $(".tab-1").removeClass("active");
    $(".tab-3").removeClass("active");
    $(".side-menu__item").removeClass("active");
    $("#tab_2_button").addClass("active");
    $(".tab").removeClass("active");
    $(".tab-2").addClass("active");
    $(".svg-display-buttons").removeClass("active");
    $(".special-adjust-tab1-footnotes").removeClass("active");
}

const handleTab3 = () => {
    $(".h1-header--tab").removeClass("active");
    $("#tab_3_header").addClass("active");
    $(".side-menu__item").removeClass("active");
    $("#tab_3_button").addClass("active");
    $(".tab").removeClass("active");
    $(".tab-1").removeClass("active");
    $(".tab-2").removeClass("active");
    $(".tab-3").addClass("active");
    $(".special-adjust-tab1-footnotes").removeClass("active");
}


const closeDialogFn = (item) => {
    let hiddenDialog = item ? item : com.gsk.mt.dialogStack[0];

    let btn = $(hiddenDialog).parent().find('button.ui-dialog-titlebar-close');

    $(btn).trigger('click');
}


com.gsk.mt.openDialog = function ($dialog) {

    if (com.gsk.mt.dialogStack.length > 0) {
        closeDialogFn();
    }

    //---Same As Original---//

    var activeQuickLink = $(".quickLinkDialogContent:not(.hidden)");
    $dialog.dialog("open");
    com.gsk.mt.dialogStack.push($dialog);
    if (com.gsk.mt.onDialogOpen !== undefined) {
        com.gsk.mt.onDialogOpen();
    }

    if (com.gsk.mt.onVideoDialogOpen !== undefined) {
        com.gsk.mt.onVideoDialogOpen();
    }

    if (activeQuickLink.hasClass('quickLinkDialogReferences')) {

    } else if (activeQuickLink.hasClass('quickLinkDialogPi')) {

    } else {
        com.gsk.mt.rebuildDialogIscroll($dialog);
    }

    if (com.gsk.mtconfig.enableQuickLinksOnDialog) {
        $(".navBottom").appendTo($(".ui-widget-overlay").last());
        if (isSafari === true) {
            resUnit();
        }
    }
}



