// --- local.js --- //

com.gsk.mt.onInit = function() {


    const toggleButton = document.querySelector('#toggle-button');
    const tapButton = new Hammer(toggleButton)
    const toggleArrow = document.querySelector('#toggle-button img');
    const slide1 = document.querySelector('.slide-1')
    const slide2 = document.querySelector('.slide-2')

    tapButton.on('tap', ()=>{
        toggleArrow.classList.toggle('rotate')
        slide1.classList.toggle('slide-active')
        slide2.classList.toggle('slide-active')
    })
	
};