All projects using the Design Center Master Template (MT) should use this template repo as a starter.  

Trust me, you will thank us later.  :)

Initial setup
=============

Run `npm install`.

Configuration
=============

## Grunt Task Runner

**`gruntfile.js`**

_Caution: Avoid Editing Directly_

This project includes some fantastic tools to scaffold your project swithout having to dig into the grunt tooling directly.

Start grunt with the command:

```sh
grunt
```

You will be prompted to select a task, these include:

* `default`
* `initializeGSKProject`
* `serve`
* `build`
* `copyGSKScaffolding`
* `saveSelections`
* `addKeyMessage`



## MT Config

**`shared/js/config.json`**

_Project-specific configuration for the master template._


Populate the `shared/js/config.json` file with project-specific information. This is necessary for navigation and for the dynamically loaded content (menu, references) to work.

Scaffolding
============

You will see a folder `MT_Scaffolding` in the project, this is the base files for the master template *do not edit these*.

Developing
==========


## MT Presentation

**`shared/js/presentation.js`**

_Project-specific presentation-global methods._

By default, this file doesn not have any options or functionality.

By convention, we may have a standard or base of reusable presentation methods here.

Note: This file is part of the built source code.  It *should not* include configuration or settings only for local-dev without precautions (e.g. wrapped in build or scompiler constants or device/environment detection).


## MT Key Messages (Slides)

### HTML
Place all slide body content inside the `<div class="mainContent"></div>`, including slide-specific modals. The rest of the page content should be elements that are common to all slides.

#### HTML partials
Page common elements are loaded from partials located in `dynamic_content/` via gruntfile.js as part of constructing a new build. Format: 

```
<!-- @import [partial] -->SOME TEXT<!-- /@import --#>
```

 The "SOME TEXT" will be replaced by the contents of a file located at `dynamic_content/_[partial].html`.

Note:  This style of inclusion should be used for build-time content replacement.  It should not be used to modify source code.


### Custom logic & behaviour

* JavaScript to be made available to all slides goes in `shared/js/presentation.js`.
* Slide specific JavaScript goes in the slide's `local.js`.

### Styling

We are using the SASS pre-processor for CSS files.   These are not included in distribution builds.

SASS files are organized along the same conventions as the master template.

* Presentation-global styling should be placed in the appropriate `shared` folder:
    - Mixins in `[MAIN]/shared/css/mixins.scss`
    - Style variables in `[MAIN]/shared/css/variables.scss`
    - Fonts in `[MAIN]/shared/css/fonts.scss`
    - Typography in `[MAIN]/shared/css/typography.scss`
    - Theme/shared styling in `[MAIN]/shared/css/presentation.scss`
* Styling for individual slides contain their own  which imports definitions (variables, animations, mixins) from the shared folder.
    - Local styles`[MAIN]/[SOME-KM]/css/local.scss`

<!-- 
// TODO double check this

- Key messages in the ADD and RES have access to MAIN's presentation stylesheet. -->

## Additional References

The GSK Master Template ships with documentation and a "strawman" to demonstarate key functionality or implementation patterns.   Smart developers read the fine manual to make their future more enjoyable... 

Note the version you are working with by checking the current repo tag or in `[MAIN]/shared/js/mt.js`, then check out:

* Optimus docs and resources: 
    - `OPTIMUS/Clients/GSK/GSK_Process_Documentation/eDetail_Guidelines`
    - `OPTIMUS/Clients/GSK/GSK_Process_Documentation/eDetail_Guidelines/Veeva_Technical_Documentation`
* Strawman template examples are available on Optimus or in Bitbucket (search for Strawman)
    - Web http://viscira3.com/gsk/Veeva_MT_Strawman_Presentation_vXXX


QC Requirements
===============

All MT projects are submitted to Desing Center for QC.  They ensure that the projects developed on behalf of our mutua clients conform with the requiremetns and expected conventions.

Do not modify any CSS or JS files other than the ones mentioned, or we will fail the Design Center QC (see Design Center RAG -- red amber green -- guidelines and notes about project structure).

Build
=====

Building the project for distribution as Veeva Key Message zip archives is managed through grunt tasks.  

Building a project should be done through Jenkins. If that has not been setup, then use the grunt tasks locally to build key message zips for distribution.

## Automated

Projects should be setup with a `Jenkinsfile` for automated deployments via Jenkins.

<!-- TODO add more once base Jenkinsfile is avialable -->

## Tooled

Run the default `grunt` task from your project root, then select `build`.  

<!-- TODO check the below.

Run the grunt `default` task to build the Veeva key messages, which will be in the dist folder. The folder should also have the necessary CTL files to initiate upload to the Veeva sandbox; if not, ask the Viscira Veeva administrator.

## Grunt tasks
- `grunt build` compiles all SASS files in the project
- `grunt` runs the default task (all of the above). -->

Distribution
============

Assuming the `default` Grunt task has been run as described above, the key messages ZIP files needed for distribution to the client will be located in the folder `dist/`.